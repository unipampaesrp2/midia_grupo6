/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visualizacao;
import Controle.EbookDAO;

/**
 * Classe com os métodos de visualizacao de Ebooks
 * @author Gustavo Rodrigues
 */
public class EbookView {
    
    /**
     * Método de visualização para adicionar um Ebook
     */
    public void mostraAdicionar(){
        EbookDAO cc = new EbookDAO();
    }
    
    /**
     * Método de visualização para alterar um Ebook
     */
    public void mostraAlterar(){
        
    }
    
    /**
     * Método de visualização para excluir um Ebook
     */
    public void mostraExcluir(){
        
    }
    
    /**
     * Método de visualização da lista de Ebooks
     */
    public void mostraListar(){
        
    }
    
}
