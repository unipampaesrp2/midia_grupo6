/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visualizacao;
import Controle.JogoDAO;

/**
 * Classe com os métodos de visualizacao de Jogos
 * @author Gustavo Rodrigues
 */
public class JogosView {
    
    /**
     * Método de visualização para adicionar um Jogo
     */
    public void mostraAdicionar(){
        JogoDAO cc = new JogoDAO();
    }
    
    /**
     * Método de visualização para alterar um Jogo
     */
    public void mostraAlterar(){
        
    }
    
    /**
     * Método de visualização para excluir um Jogo
     */
    public void mostraExcluir(){
        
    }
    
    /**
     * Método de visualização da lista de Jogos
     */
    public void mostraListar(){
        
    }
    
}
