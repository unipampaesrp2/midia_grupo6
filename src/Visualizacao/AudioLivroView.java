package Visualizacao;

import Controle.AudioLivroDAO;

/**
 * Classe com os métodos de visualizacao do AudioLivro.
 * @author Bruno Ramalho
 */
public class AudioLivroView {
    
    /**
     * Método de visualização para adicionar um AudioLivro.
     */
    public void mostraAdicionar(){
        AudioLivroDAO bb = new AudioLivroDAO();
    }
    
    /**
     * Método de visualização para alterar um AudioLivro.
     */
    public void mostraAlterar(){
        
    }
    
    /**
     * Método de visualização para excluir um AudioLivro.
     */
    public void mostraExcluir(){
        
    }
    
    /**
     * Método de visualização da lista de AudioLivro.
     */
    public void mostraListar(){
        
    }
    
}
