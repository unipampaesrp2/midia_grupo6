/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visualizacao;

import Controle.MusicaDAO;

/**
 * Classe com metodos de visualizacao
 * @author NoteCCE
 */
public class MusicaView {
    
    /**
     * Metodo de visualizacao para adicionar uma musica
     */
    public void mostraAdicionar(){
        MusicaDAO m = new MusicaDAO();
    }
    
    /**
     * Metodo de visualizacao para alterar uma musica
     */
    public void mostraAlterar(){
        
    }
    
    /**
     * Metodo de visualizacao para excluir uma musica
     */
    public void mostraExcluir(){
        
    }
    
    /**
     * Metodo para visualizar a listagem das musicas
     */
    public void mostraListar(){
        
    }
    
}
