package Visualizacao;

import Controle.FilmeDAO;

/**
 * Classe com os métodos de visualizacao do Filme.
 * @author Bruno Ramalho
 */
public class FilmeView {
    
    /**
     * Método de visualização para adicionar um Filme.
     */
    public void mostraAdicionar(){
        FilmeDAO cc = new FilmeDAO();
    }
    
    /**
     * Método de visualização para alterar um Filme.
     */
    public void mostraAlterar(){
        
    }
    
    /**
     * Método de visualização para excluir um Filme.
     */
    public void mostraExcluir(){
        
    }
    
    /**
     * Método de visualização da lista de Filme.
     */
    public void mostraListar(){
        
    }
    
}
