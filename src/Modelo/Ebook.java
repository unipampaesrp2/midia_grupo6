/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Gustavo Rodrigues
 */
public class Ebook extends Midia{
    
    private String genero;
    private Pessoa autor;
    private String local;
    private String editora;
    private int numPaginas;
    private int ano;
    
    public Ebook(){
        
    }
    
    public Ebook(Pessoa p){
        this.autor = p;
    }

    /**
     * @return the gen
     */
    public String getGen() {
        return genero;
    }

    /**
     * @param gen the gen to set
     */
    public void setGen(String gen) {
        this.genero = gen;
    }

    /**
     * @return the autores
     */
    public Pessoa getAutor() {
        return autor;
    }

    /**
     * @param autores the autores to set
     */
    public void setAutor(Pessoa autor) {
        this.autor = autor;
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * @return the editora
     */
    public String getEditora() {
        return editora;
    }

    /**
     * @param editora the editora to set
     */
    public void setEditora(String editora) {
        this.editora = editora;
    }

    /**
     * @return the numPaginas
     */
    public int getNumPaginas() {
        return numPaginas;
    }

    /**
     * @param numPaginas the numPaginas to set
     */
    public void setNumPaginas(int numPaginas) {
        this.numPaginas = numPaginas;
    }

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }
    
}
