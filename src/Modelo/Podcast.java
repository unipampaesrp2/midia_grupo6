/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author NoteCCE
 */
public class Podcast extends Midia{
    
    private String idioma;
    private Pessoa autor;
    private int ano;
    
    public Podcast(){
        
    }
    
    public Podcast(Pessoa n){
        this.autor = n;
    }

    /**
     * @return the idioma
     */
    public String getIdioma() {
        return idioma;
    }

    /**
     * @param idioma the idioma to set
     */
    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    /**
     * @return the autor
     */
    public Pessoa getAutor() {
        return autor;
    }

    /**
     * @param autor the autor to set
     */
    public void setAutor(Pessoa autor) {
        this.autor = autor;
    }

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }
    
}
