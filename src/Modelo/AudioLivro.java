package Modelo;

/**
 *
 * @author Bruno Ramalho
 */

public class AudioLivro extends Midia{
    private String generoAudioLivro;
    private String idiomaAudioLivro;
    private Pessoa diretorAudioLivro;
    private Pessoa autorAudioLivro;
    private String localAudioLivro;
    private String editoraAudioLivro;
    private double duracaoAudioLivro;
    private int anoAudioLivro;
    
    public AudioLivro(){
        
        
    }

    /**
     * @return the generoAudioLivro
     */
    public String getGeneroAudioLivro() {
        return generoAudioLivro;
    }
    
    /**
     * @param generoAudioLivro the generoAudioLivro to set
     */
    public void setGeneroAudioLivro(String generoAudioLivro) {
        this.generoAudioLivro = generoAudioLivro;
    }

    /**
     * @return the idiomaAudioLivro
     */
    public String getIdiomaAudioLivro() {
        return idiomaAudioLivro;
    }
    
    /**
     * @param idiomaAudioLivro the idiomaAudioLivro to set
     */
    public void setIdiomaAudioLivro(String idiomaAudioLivro) {
        this.idiomaAudioLivro = idiomaAudioLivro;
    }

    /**
     * @return the diretorAudioLivro
     */
    public Pessoa getDiretorAudioLivro() {
        return diretorAudioLivro;
    }

    /**
     * @param diretorAudioLivro the diretorAudioLivro to set
     */
    public void setDiretorAudioLivro(Pessoa diretorAudioLivro) {
        this.diretorAudioLivro = diretorAudioLivro;
    }

    /**
     * @return the autorAudioLivro
     */
    public Pessoa getAutorAudioLivro() {
        return autorAudioLivro;
    }

    /**
     * @param autorAudioLivro the autorAudioLivro to set
     */
    public void setAutorAudioLivro(Pessoa autorAudioLivro) {
        this.autorAudioLivro = autorAudioLivro;
    }

    /**
     * @return the localAudioLivro
     */
    public String getLocalAudioLivro() {
        return localAudioLivro;
    }

    /**
     * @param localAudioLivro the localAudioLivro to set
     */
    public void setLocalAudioLivro(String localAudioLivro) {
        this.localAudioLivro = localAudioLivro;
    }

    /**
     * @return the editoraAudioLivro
     */
    public String getEditoraAudioLivro() {
        return editoraAudioLivro;
    }

    /**
     * @param editoraAudioLivro the editoraAudioLivro to set
     */
    public void setEditoraAudioLivro(String editoraAudioLivro) {
        this.editoraAudioLivro = editoraAudioLivro;
    }

    /**
     * @return the duracaoAudioLivro
     */
    public double getDuracaoAudioLivro() {
        return duracaoAudioLivro;
    }

    /**
     * @param duracaoAudioLivro the duracaoAudioLivro to set
     */
    public void setDuracaoAudioLivro(int duracaoAudioLivro) {
        this.duracaoAudioLivro = duracaoAudioLivro;
    }

    /**
     * @return the anoAudioLivro
     */
    public int getAnoAudioLivro() {
        return anoAudioLivro;
    }

    /**
     * @param anoAudioLivro the anoAudioLivro to set
     */
    public void setAnoAudioLivro(int anoAudioLivro) {
        this.anoAudioLivro = anoAudioLivro;
    }
    
}
