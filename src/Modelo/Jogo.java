/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Gustavo Rodrigues
 */
public class Jogo extends Midia{
    
    private String genero;
    private Pessoa autor;
    private int numJogador;
    private boolean suporteRede;
    private int ano;
    
    public Jogo(){
        
    }
    
    public Jogo(Pessoa p){
        this.autor = p;
    }

    /**
     * @return the gen
     */
    public String getGenero() {
        return genero;
    }

    /**
     * @param gen the gen to set
     */
    public void setGen(String gen) {
        this.genero = gen;
    }

    /**
     * @return the autor
     */
    public Pessoa getAutor() {
        return autor;
    }

    /**
     * @param autor the autor to set
     */
    public void setAutor(Pessoa autor) {
        this.autor = autor;
    }

    /**
     * @return the numJogador
     */
    public int getNumJogador() {
        return numJogador;
    }

    /**
     * @param numJogador the numJogador to set
     */
    public void setNumJogador(int numJogador) {
        this.numJogador = numJogador;
    }

    /**
     * @return the supRede
     */
    public boolean isSupRede() {
        return suporteRede;
    }

    /**
     * @param supRede the supRede to set
     */
    public void setSupRede(boolean supRede) {
        this.suporteRede = supRede;
    }

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }
    
    
}
