package Modelo;

/**
 * @author Bruno Ramalho
 */

public class Filme extends Midia{
    
    private String generoFilme;
    private String idiomaFilme;
    private Pessoa diretorFilme;
    private Pessoa atorPrinc;
    private int duracaoFilme;
    private int anoFilme;
    
    public Filme(){
        
        
    }

    /**
     * @return the generoFilme
     */
    public String getGeneroFilme() {
        return generoFilme;
    }

    /**
     * @param generoFilme the generoFilme to set
     */
    public void setGeneroFilme(String generoFilme) {
        this.generoFilme = generoFilme;
    }

    /**
     * @return the idiomaFilme
     */
    public String getIdiomaFilme() {
        return idiomaFilme;
    }

    /**
     * @param idiomaFilme the idiomaFilme to set
     */
    public void setIdiomaFilme(String idiomaFilme) {
        this.idiomaFilme = idiomaFilme;
    }

    /**
     * @return the diretorFilme
     */
    public Pessoa getDiretorFilme() {
        return diretorFilme;
    }

    /**
     * @param diretorFilme the diretorFilme to set
     */
    public void setDiretorFilme(Pessoa diretorFilme) {
        this.diretorFilme = diretorFilme;
    }

    /**
     * @return the atorPrinc
     */
    public Pessoa getAtorPrinc() {
        return atorPrinc;
    }

    /**
     * @param atorPrinc the atorPrinc to set
     */
    public void setAtorPrinc(Pessoa atorPrinc) {
        this.atorPrinc = atorPrinc;
    }

    /**
     * @return the duracaoFilme
     */
    public int getDuracaoFilme() {
        return duracaoFilme;
    }

    /**
     * @param duracaoFilme the duracaoFilme to set
     */
    public void setDuracaoFilme(int duracaoFilme) {
        this.duracaoFilme = duracaoFilme;
    }

    /**
     * @return the anoFilme
     */
    public int getAnoFilme() {
        return anoFilme;
    }

    /**
     * @param anoFilme the anoFilme to set
     */
    public void setAnoFilme(int anoFilme) {
        this.anoFilme = anoFilme;
    }
    
    
}
