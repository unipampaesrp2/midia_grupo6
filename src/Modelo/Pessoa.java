/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author NoteCCE
 */
public class Pessoa {
    
    private String nome;
    
    /**
     * Metodo construtor
     * @param n 
     */
    public Pessoa(String n){
        this.nome = n;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    
}
