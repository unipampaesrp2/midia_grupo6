/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author NoteCCE
 */
public class Musica extends Midia {
    
    private String genero;
    private String idioma;
    private Pessoa autor;
    private Pessoa interprete;
    private double duracao;
    private int ano;
    
    
    public Musica(){
        
    }
    
    public Musica (Pessoa n){
        this.interprete = n;
        this.autor = n;
       
    }

    /**
     * @return the idioma
     */
    public String getIdioma() {
        return idioma;
    }

    /**
     * @param idioma the idioma to set
     */
    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    /**
     * @return the autor
     */
    public Pessoa getAutor() {
        return autor;
    }

    /**
     * @param autor the autor to set
     */
    public void setAutor(Pessoa autor) {
        this.autor = autor;
    }

    /**
     * @return the interprete
     */
    public Pessoa getInterprete() {
        return interprete;
    }

    /**
     * @param interprete the interprete to set
     */
    public void setInterprete(Pessoa interprete) {
        this.interprete = interprete;
    }

    /**
     * @return the duracao
     */
    public double getDuracao() {
        return duracao;
    }

    /**
     * @param duracao the duracao to set
     */
    public void setDuracao(double duracao) {
        this.duracao = duracao;
    }

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }
}
