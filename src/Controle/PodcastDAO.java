/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import Modelo.Podcast;
import Modelo.Pessoa;

/**
 * Classe com metodos de controle
 * @author NoteCCE
 */
public class PodcastDAO {
    
    /**
     * Metodo que adiciona um Podcast no banco de midias
     */
    public void adicionar(){
        Podcast p = new Podcast();
        
    }
    
    /**
     * Metodo que altera os dados de um Podcast armazenado no banco de midias
     * @param titulo 
     */
    public void alterar(String titulo){
        
    }
    
    /**
     * Metodo que excluir um Podcast do banco de midias
     * @param titulo 
     */
    public void excluir(String titulo){
        
    }
    
    /**
     * Metodo que lsita os Podcast existentes no banco de midias
     */
    public void listar(){
        
    }
    
    
    /**
     * Metodo responsavel por ordenr os Podcasts existentes no banco
     * de midias pelo genero
     * @param genero 
     */
    public void ordenar(String genero){
        
    }
    
    /**
       * Metodo responsavel por ordenr os Podcasts existentes no banco
     * de midias pelo autor
     * @param autor 
     */
    public void ordenar(Pessoa autor){
        
    }
}
