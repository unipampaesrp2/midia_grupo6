package Controle;

import Modelo.AudioLivro;
import Modelo.Pessoa;

/**
 * Classe com os métodos de controle do AudioLivro.
 * @author Bruno Ramalho
 */
public class AudioLivroDAO {
    
    /**
     * Método para adicionar filme.
     */
    public void adicionar(){
        AudioLivro AudioLivro = new AudioLivro();
    }
    
    /**
     * Método que executa alteracões de um AudioLivro incluído.
     * @param titulo 
     */
    public void alterar(String titulo){
        
    }
    
    /**
     * Método para exlcuir um AudioLivro.
     * @param titulo 
     */
    public void excluir(String titulo){
        
    }
    
    /**
     * Método para listar os AudioLivros.
     */
    public void listar(){
        
    }
    
    /**
     * Metodo que ordena os AudioLivro pelo gênero.
     * @param genero 
     */
    public void ordenar(String genero){
        
    }
    
    /**
     * Metodo que ordena os AudioLivros pelos autores.
     * @param autorAudioLivro 
     */
    public void ordenar(Pessoa autorAudioLivro){
        
    }
    
}
