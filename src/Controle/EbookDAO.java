package Controle;

import Modelo.Ebook;
import Modelo.Pessoa;

/**
 * Classe com métodos de controle do Ebook
 * @author Gustavo Rodrigues
 */
public class EbookDAO {
    
    /**
     * Adicionar um novo ebook na lista
     */
    public void adiciona(){
        Ebook e = new Ebook();
    }
    
    /**
     * Altera um ebook já adicionado
     * @param titulo 
     */
    public void altera(String titulo){
        
    }
    
    /**
     * Exclui um ebook
     * @param titulo 
     */
    public void excluir(String titulo){
        
    }
    
    /**
     * Lista os ebooks
     */
    public void listar(){
        
    }
    
    /**
     * Metodo que ordena os ebook pelos autores
     * @param autor 
     */
    public void ordenar(Pessoa autor){
        
    }
    
}
