/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import Modelo.Musica;
import Modelo.Pessoa;

/**
 * Classe com os metodos de controle
 * @author NoteCCE
 */
public class MusicaDAO {
    
    /**
     * Metodo para adicionar uma nova musica
     */
    public void adiciona(){
        Musica m = new Musica();
    }
    
    /**
     * Medoto que faz a alteracao de uma musica ja adicionada
     * @param titulo 
     */
    public void altera(String titulo){
        
    }
    
    /**
     * Metodo para exlcuir uma musica
     * @param titulo 
     */
    public void excluir(String titulo){
        
    }
    
    /**
     * Metodo para listar as musicas
     */
    public void listar(){
        
    }
    
    /**
     * Metodo que ordena as musicas pelo genero musical
     * @param genero 
     */
    public void ordenar(String genero){
        
    }
    
    /**
     * Metodo que ordena as musicas pelos autores
     * @param autor 
     */
    public void ordenar(Pessoa autor){
        
    }
    
}
