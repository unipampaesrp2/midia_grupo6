package Controle;

import Modelo.Jogo;
import Modelo.Pessoa;

/**
 * Classe com métodos de controle de Jogo
 * @author Gustavo Rodrigues
 */
public class JogoDAO {
        
    /**
     * Adicionar um novo jogo na lista
     */
    public void adiciona(){
        Jogo j = new Jogo();
    }
    
    /**
     * Altera um jogo já adicionado
     * @param titulo 
     */
    public void altera(String titulo){
        
    }
    
    /**
     * Exclui uma música
     * @param titulo 
     */
    public void excluir(String titulo){
        
    }
    
    /**
     * Lista as músicas
     */
    public void listar(){
        
    }
    
    /**
     * Metodo que ordena os jogos pela conectividade na rede
     * @param rede 
     */
    public void ordenar(boolean rede){
        
    }
    
    /**
     * Metodo que ordena os jogos pela número de jogadores
     * @param numJogador
     */
    public void ordenar(int numJogador){
        
    }
    
}
