package Controle;

import Modelo.Filme;
import Modelo.Pessoa;

/**
 * Classe com os métodos de controle do Filme.
 * @author Bruno Ramalho
 */
public class FilmeDAO {
    
    /**
     * Método para adicionar filme.
     */
    public void adicionar(){
        Filme Film = new Filme();
    }
    
    /**
     * Método que executa alteracões de um filme incluído.
     * @param titulo 
     */
    public void alterar(String titulo){
        
    }
    
    /**
     * Método para exlcuir um filme.
     * @param titulo 
     */
    public void excluir(String titulo){
        
    }
    
    /**
     * Método para listar os filmes.
     */
    public void listar(){
        
    }
    
    /**
     * Metodo que ordena os filmes pelos gêneros.
     * @param genero 
     */
    public void ordenar(String genero){
        
    }
    
    /**
     * Metodo que ordena os filmes por diretor.
     * @param diretorFilme 
     */
    public void ordenar(Pessoa diretorFilme){
        
    }
    
    /**
     * Método que ordena os filmes pelo ator.
     * @param atorPrinc 
     */
    public void ordena(Pessoa atorPrinc){
        
    }
    
}
